﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LetacoSorteo
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        UsuarioBL usuarioBL;

        public void iniciarSesion(string usuario, string clave)
        {            
            Usuario usuarioRespuesta;

            if (EstaticoSistema.usuarioAdminSistema.nombreUsuario == usuario &&
                EstaticoSistema.usuarioAdminSistema.claveUsuario == clave)
            {
                usuarioRespuesta = EstaticoSistema.usuarioAdminSistema;
            }
            else
            {
                Usuario usuarioEnvio = new Usuario() { nombreUsuario = usuario, claveUsuario = clave };
                usuarioRespuesta = usuarioBL.Login(usuarioEnvio);
            }             

            txtusuario.Clear();
            txtclave.Clear();

            if (usuarioRespuesta.exito)
            {
                EstaticoSistema.usuarioSistema = usuarioRespuesta;
                this.Hide();
                Principal frmPrincipal = new Principal();
                frmPrincipal.ShowDialog();
                this.Visible = true;                
            }
            else
            {
                lblmensajeError.Text = usuarioRespuesta.respuesta;
                lblmensajeError.Visible = true;
                txtusuario.Focus();
            }
            
        }

        private void btnaceptar_Click(object sender, EventArgs e)
        {
            iniciarSesion(txtusuario.Text, txtclave.Text);
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnminimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Login_Load(object sender, EventArgs e)
        {
            usuarioBL = new UsuarioBL();
        }

        private void pbxverclave_MouseMove(object sender, MouseEventArgs e)
        {
            txtclave.UseSystemPasswordChar = false;
        }

        private void pbxverclave_MouseLeave(object sender, EventArgs e)
        {
            txtclave.UseSystemPasswordChar = true;
        }
    }
}
