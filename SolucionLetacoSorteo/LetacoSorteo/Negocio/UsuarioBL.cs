﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LetacoSorteo
{
    public class UsuarioBL
    {
        UsuarioDAL usuarioDAL;
        public UsuarioBL() {
            usuarioDAL = new UsuarioDAL();
        }

        public Usuario Login(Usuario usuario)
        {
            Usuario usuarioRespuesta = new Usuario();
            if (usuario.nombreUsuario == string.Empty)
            {
                usuarioRespuesta.respuesta = "Usuario no ingresado, por favor ingrese usuario";
                return usuarioRespuesta;
            }
            if (usuario.claveUsuario == string.Empty)
            {
                usuarioRespuesta.respuesta = "Clave no ingresada, por favor ingrese clave";
                return usuarioRespuesta;
            }

            usuarioRespuesta = usuarioDAL.Login(usuario);

            return usuarioRespuesta;
        }
    }
}
