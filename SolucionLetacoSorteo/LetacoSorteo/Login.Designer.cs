﻿namespace LetacoSorteo
{
    partial class Login
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.gbxIniciarSesion = new System.Windows.Forms.GroupBox();
            this.pbxverclave = new System.Windows.Forms.PictureBox();
            this.lblmensajeError = new System.Windows.Forms.Label();
            this.btnminimizar = new System.Windows.Forms.Button();
            this.btnsalir = new System.Windows.Forms.Button();
            this.btnaceptar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtclave = new System.Windows.Forms.TextBox();
            this.txtusuario = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbxIniciarSesion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxverclave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // gbxIniciarSesion
            // 
            this.gbxIniciarSesion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxIniciarSesion.BackColor = System.Drawing.Color.Transparent;
            this.gbxIniciarSesion.Controls.Add(this.pbxverclave);
            this.gbxIniciarSesion.Controls.Add(this.lblmensajeError);
            this.gbxIniciarSesion.Controls.Add(this.btnminimizar);
            this.gbxIniciarSesion.Controls.Add(this.btnsalir);
            this.gbxIniciarSesion.Controls.Add(this.btnaceptar);
            this.gbxIniciarSesion.Controls.Add(this.label2);
            this.gbxIniciarSesion.Controls.Add(this.txtclave);
            this.gbxIniciarSesion.Controls.Add(this.txtusuario);
            this.gbxIniciarSesion.Controls.Add(this.pictureBox1);
            this.gbxIniciarSesion.Controls.Add(this.label1);
            this.gbxIniciarSesion.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.gbxIniciarSesion.Location = new System.Drawing.Point(14, 6);
            this.gbxIniciarSesion.Name = "gbxIniciarSesion";
            this.gbxIniciarSesion.Size = new System.Drawing.Size(519, 308);
            this.gbxIniciarSesion.TabIndex = 0;
            this.gbxIniciarSesion.TabStop = false;
            // 
            // pbxverclave
            // 
            this.pbxverclave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxverclave.Image = ((System.Drawing.Image)(resources.GetObject("pbxverclave.Image")));
            this.pbxverclave.Location = new System.Drawing.Point(300, 149);
            this.pbxverclave.Name = "pbxverclave";
            this.pbxverclave.Size = new System.Drawing.Size(42, 26);
            this.pbxverclave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxverclave.TabIndex = 7;
            this.pbxverclave.TabStop = false;
            this.pbxverclave.MouseLeave += new System.EventHandler(this.pbxverclave_MouseLeave);
            this.pbxverclave.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbxverclave_MouseMove);
            // 
            // lblmensajeError
            // 
            this.lblmensajeError.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblmensajeError.AutoSize = true;
            this.lblmensajeError.BackColor = System.Drawing.Color.DarkRed;
            this.lblmensajeError.Font = new System.Drawing.Font("Segoe UI", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.lblmensajeError.ForeColor = System.Drawing.Color.White;
            this.lblmensajeError.Location = new System.Drawing.Point(29, 275);
            this.lblmensajeError.Name = "lblmensajeError";
            this.lblmensajeError.Size = new System.Drawing.Size(213, 19);
            this.lblmensajeError.TabIndex = 0;
            this.lblmensajeError.Text = "El Usuario igresado no es válido";
            this.lblmensajeError.Visible = false;
            // 
            // btnminimizar
            // 
            this.btnminimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnminimizar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnminimizar.BackgroundImage")));
            this.btnminimizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnminimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnminimizar.FlatAppearance.BorderSize = 0;
            this.btnminimizar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnminimizar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnminimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnminimizar.Location = new System.Drawing.Point(423, 17);
            this.btnminimizar.Name = "btnminimizar";
            this.btnminimizar.Size = new System.Drawing.Size(41, 38);
            this.btnminimizar.TabIndex = 6;
            this.btnminimizar.UseVisualStyleBackColor = true;
            this.btnminimizar.Click += new System.EventHandler(this.btnminimizar_Click);
            // 
            // btnsalir
            // 
            this.btnsalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnsalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnsalir.BackgroundImage")));
            this.btnsalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnsalir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnsalir.FlatAppearance.BorderSize = 0;
            this.btnsalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnsalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnsalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsalir.Location = new System.Drawing.Point(472, 17);
            this.btnsalir.Name = "btnsalir";
            this.btnsalir.Size = new System.Drawing.Size(41, 38);
            this.btnsalir.TabIndex = 5;
            this.btnsalir.UseVisualStyleBackColor = true;
            this.btnsalir.Click += new System.EventHandler(this.btnsalir_Click);
            // 
            // btnaceptar
            // 
            this.btnaceptar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnaceptar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnaceptar.FlatAppearance.BorderSize = 0;
            this.btnaceptar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnaceptar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnaceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnaceptar.Font = new System.Drawing.Font("Segoe UI", 13F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.btnaceptar.ForeColor = System.Drawing.Color.Black;
            this.btnaceptar.Location = new System.Drawing.Point(369, 229);
            this.btnaceptar.Name = "btnaceptar";
            this.btnaceptar.Size = new System.Drawing.Size(134, 49);
            this.btnaceptar.TabIndex = 4;
            this.btnaceptar.Text = "Iniciar Sesión";
            this.btnaceptar.UseVisualStyleBackColor = true;
            this.btnaceptar.Click += new System.EventHandler(this.btnaceptar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 13F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(238, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Clave";
            // 
            // txtclave
            // 
            this.txtclave.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.txtclave.Location = new System.Drawing.Point(238, 182);
            this.txtclave.Name = "txtclave";
            this.txtclave.Size = new System.Drawing.Size(224, 29);
            this.txtclave.TabIndex = 3;
            this.txtclave.Text = "soporteLetaco";
            this.txtclave.UseSystemPasswordChar = true;
            // 
            // txtusuario
            // 
            this.txtusuario.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.txtusuario.Location = new System.Drawing.Point(238, 109);
            this.txtusuario.Name = "txtusuario";
            this.txtusuario.Size = new System.Drawing.Size(224, 29);
            this.txtusuario.TabIndex = 2;
            this.txtusuario.Text = "soporteLetaco";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(30, 71);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(172, 149);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 13F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(238, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Usuario";
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(547, 329);
            this.Controls.Add(this.gbxIniciarSesion);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Iniciar Sesión";
            this.Load += new System.EventHandler(this.Login_Load);
            this.gbxIniciarSesion.ResumeLayout(false);
            this.gbxIniciarSesion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxverclave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxIniciarSesion;
        private System.Windows.Forms.TextBox txtusuario;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnsalir;
        private System.Windows.Forms.Button btnaceptar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtclave;
        private System.Windows.Forms.Button btnminimizar;
        private System.Windows.Forms.Label lblmensajeError;
        private System.Windows.Forms.PictureBox pbxverclave;
    }
}

