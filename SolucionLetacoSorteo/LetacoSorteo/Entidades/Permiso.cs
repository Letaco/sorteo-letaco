﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LetacoSorteo
{
    public class Permiso
    {
        public int Id { get; set; }

        public string nombreMostrar { get; set; }

        public string nombreControl { get; set; }

        public Usuario usuarioCrea { get; set; }

        public Usuario usuarioActualiza { get; set; }

        public DateTime fechacrea { get; set; }

        public DateTime fechaActualiza { get; set; }

        public int estado { get; set; }

        //variables adicionales
        public int IdUsuarioCrea { get; set; }
        public int IdUsuarioActualiza { get; set; }
    }
}
