﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LetacoSorteo
{
    public class Usuario_Permiso
    {
        public int Id { get; set; }

        public Usuario usuario { get; set; }

        public Permiso permiso { get; set; }

        public int tipoPermiso { get; set; }

        public Usuario usuarioCrea { get; set; }

        public Usuario usuarioActualiza { get; set; }

        public DateTime fechaCreacion { get; set; }

        public DateTime fechaActualiza { get; set; }

        public int estado { get; set; }

        //variables adicionales
        public int IdPermiso { get; set; }
        public int IdUsuario { get; set; }
        public int IdUsuarioCrea { get; set; }
        public int IdUsuarioActualiza { get; set; }
        public string usuarioDescripcion { get; set; }
        public string usuarioCreaNombre { get; set; }

        public string usuarioActualizaNombre { get; set; }

        public string permisoDescripción { get; set; }
    }
}
