﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace LetacoSorteo
{
    public class Usuario
    {
        // variables de tabla
        public int Id { get; set; }
        public string nombreUsuario { get; set; }
        public string claveUsuario { get; set; }
        public Image imagenUsuario { get; set; }
        public Usuario usuarioCrea { get; set; }
        public Usuario usuarioActualiza { get; set; }
        public DateTime fechaCreacion { get; set; }
        public DateTime fechaActualiza { get; set; }
        public int estado { get; set; }
        
        //variables de retorno y validación
        public string usuarioCreaDescripcion { get; set; }
        public int usuarioCreaId { get; set; }
        public string usuarioActualizaDescripcion { get; set; }
        public int usuarioActualizaId { get; set; }

        public string respuesta = string.Empty;

        public bool exito = false;
    }
}
