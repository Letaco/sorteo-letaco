﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace LetacoSorteo
{
    public partial class Inicio : Form
    {
        public Inicio()
        {
            InitializeComponent();            
        }

        Timer Timer;
        int numeroFin = 0;

        private void Principal_Load(object sender, EventArgs e)
        {
            Timer = new Timer();
            Timer.Tick += Timer_Tick;
            Timer.Interval = EstaticoSistema.tiempocargaavance;
            Timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (numeroFin < EstaticoSistema.tiempocarga)
                numeroFin += Timer.Interval;
            else
            {
                Timer.Stop();
                this.Hide();
                Login pantallaInicio = new Login();
                pantallaInicio.ShowDialog();
                this.Close();
            }

        }
    }
}
