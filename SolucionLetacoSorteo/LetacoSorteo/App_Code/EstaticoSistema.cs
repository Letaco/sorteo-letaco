﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using Microsoft.Data.SqlClient;

namespace LetacoSorteo
{
    public static class EstaticoSistema
    {
        public static int tiempocarga = 2000;
        public static int tiempocargaavance = 500;
        public static int intervalminute = 1000;
        public static Usuario usuarioSistema;
        public static Usuario usuarioAdminSistema = new Usuario()
        {
            nombreUsuario = "soporteLetaco",
            claveUsuario = "soporteLetaco",
            estado = 1,
            imagenUsuario = LetacoSorteo.Properties.Resources.Login,
            Id = 0,
            exito = true
        };
        static string cadenaConexion = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\BDSorteo.mdf;Integrated Security=True";
        public static SqlConnection connectionSQL;
        public static List<string> formularioscargados;

        public static byte[] ImageToByteArray(Image image)
        {
            using (var ms = new MemoryStream())
            {
                image.Save(ms, ImageFormat.Gif);
                return ms.GetBuffer();
            }
        }

        public static Image ByteArrayToImage(byte[] byteArray)
        {
            MemoryStream ms = new MemoryStream(byteArray);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        public static SqlConnection devolverInstanciaSQLConnection()
        {
            if (connectionSQL == null)
                connectionSQL = new SqlConnection(cadenaConexion);
            return connectionSQL;
        }

        public static bool verificarCadenaIngresada(string cadena)
        {
            if (cadena.Contains(" or ")) return false;
            if (cadena.Contains(" and ")) return false;
            if (cadena.Contains(" || ")) return false;
            if (cadena.Contains(" && ")) return false;
            if (cadena.Contains(" not ")) return false;
            if (cadena.Contains(" = ")) return false;

            return true;
        }
    }
}
