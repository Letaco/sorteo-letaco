﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace LetacoSorteo
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            newtimer.Stop();
            this.Close();
        }

        private void btnminimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        Timer newtimer;
        private void Principal_Load(object sender, EventArgs e)
        {
            lblfechaHora.Text = "Hoy es:  " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
            newtimer = new Timer();
            newtimer.Interval = EstaticoSistema.intervalminute;
            newtimer.Tick += Newtimer_Tick;
            newtimer.Start();
            asignarEventoClick();
            EstaticoSistema.formularioscargados = new List<string>();
            pbxImagenUsuario.Image = EstaticoSistema.usuarioSistema.imagenUsuario;
            lblNombreUsuario.Text = EstaticoSistema.usuarioSistema.nombreUsuario;
        }

        public void asignarEventoClick()
        {
            //imágenes
            pbxProductos.Click += LlamarFormularioImagen_Click;
            pbxUsuarios.Click += LlamarFormularioImagen_Click;
            pbxCrearSorteo.Click += LlamarFormularioImagen_Click;
            pbxRealizarSorteo.Click += LlamarFormularioImagen_Click;
            pbxHistorialSorteo.Click += LlamarFormularioImagen_Click;
            pbxHistorialParticipante.Click += LlamarFormularioImagen_Click;
            pbxReporteVentas.Click += LlamarFormularioImagen_Click;

            //textos
            lblProductos.Click += LlamarFormularioTexto_Click;
            lblusuarios.Click += LlamarFormularioTexto_Click;
            lblCrearSorteo.Click += LlamarFormularioTexto_Click;
            lblRealizarSorteo.Click += LlamarFormularioTexto_Click;
            lblHistorialSorteo.Click += LlamarFormularioTexto_Click;
            lblHistorialParticipante.Click += LlamarFormularioTexto_Click;
            lblReporteVentas.Click += LlamarFormularioTexto_Click;
        }

        private void LlamarFormularioTexto_Click(object sender, EventArgs e)
        {
            try
            {
                cargarFormulario(((Label)sender).Tag.ToString());
            }
            catch (Exception){}
            
        }

        private void LlamarFormularioImagen_Click(object sender, EventArgs e)
        {
            try
            {
                cargarFormulario(((PictureBox)sender).Tag.ToString());
            }
            catch (Exception) { }
            
        }

        private void Newtimer_Tick(object sender, EventArgs e)
        {
            lblfechaHora.Text = "Hoy es:  " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
            this.Update();
        }

        public void cargarFormulario(string nombreformulario)
        {
            if (!string.IsNullOrEmpty(nombreformulario))
            {  

                if (!EstaticoSistema.formularioscargados.Exists(x => x == nombreformulario))
                {
                    Assembly asm = Assembly.GetEntryAssembly();
                    Type formtype = asm.GetType(string.Format("LetacoSorteo.{0}", nombreformulario));

                    Form f = (Form)Activator.CreateInstance(formtype);
                    f.Dock = DockStyle.Fill;
                    f.TopLevel = false;
                    gbxOperaciones.Controls.Add(f);
                    EstaticoSistema.formularioscargados.Add(nombreformulario);
                    f.Show();
                }
                else
                {
                    MessageBox.Show("Formulario ya está cargado","Advertencia",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                }                
            }
            
        }
    }
}
