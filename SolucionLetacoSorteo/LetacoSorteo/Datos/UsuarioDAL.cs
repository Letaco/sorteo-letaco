﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Microsoft.Data.SqlClient;

namespace LetacoSorteo
{
    public class UsuarioDAL
    {
        public UsuarioDAL() { }

        public Usuario Login(Usuario usuario)
        {

            Usuario usuarioRespuesta = new Usuario();
            string sentencia = "";
            if (EstaticoSistema.verificarCadenaIngresada(usuario.nombreUsuario) &&
                EstaticoSistema.verificarCadenaIngresada(usuario.claveUsuario))
            {
                sentencia = "select us.Id, us.nombreUsuario, us.claveUsuario, us.imagenUsuario," +
                            " us.usuarioCrea, us.usuarioActualiza, us.fechaCreacion, us.fechaActualiza" +
                            " from Usuario us where us.nombreUsuario = '" + usuario.nombreUsuario +
                            "' and us.claveUsuario = '" + usuario.claveUsuario + "' and us.estado = 1";

                using (SqlConnection sqlConnection = EstaticoSistema.devolverInstanciaSQLConnection())                
                {
                    using (SqlCommand cmd = new SqlCommand(sentencia, sqlConnection))
                    {
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataSet dt = new DataSet();
                        da.Fill(dt);

                        if (dt != null && dt.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow itemtabla in dt.Tables[0].Rows)
                            {
                                usuarioRespuesta.Id = (int)itemtabla[0];
                                usuarioRespuesta.nombreUsuario = (string)itemtabla[1];
                                usuarioRespuesta.claveUsuario = (string)itemtabla[2];
                                try
                                {
                                    usuarioRespuesta.imagenUsuario = EstaticoSistema.ByteArrayToImage((byte[])itemtabla[3]);
                                }
                                catch
                                {
                                    usuarioRespuesta.imagenUsuario = null;
                                }
                                usuarioRespuesta.usuarioCrea = obtenerUsuarioById((int)itemtabla[4], 0);
                                usuarioRespuesta.usuarioActualiza = obtenerUsuarioById((int)itemtabla[5], 0);
                                usuarioRespuesta.fechaCreacion = (DateTime)itemtabla[6];
                                usuarioRespuesta.fechaActualiza = (DateTime)itemtabla[7];
                                usuarioRespuesta.exito = true;
                            }

                        }
                        else {
                            usuarioRespuesta.respuesta = "Credenciales incorrectas - Intente nuevamente";
                        }

                    }
                }

            }
            else
            {
                usuarioRespuesta.respuesta = "Datos INDEVIDOS dentro de credenciales ingresadas";
            }

            return usuarioRespuesta;
        }

        public Usuario obtenerUsuarioById(int idUsuario, int opcionDetalle)
        {
            Usuario usuarioRespuesta = new Usuario();
            string sentencia = "select us.Id, us.nombreUsuario, us.claveUsuario, us.imagenUsuario," +
                            " us.usuarioCrea, us.usuarioActualiza, us.fechaCrea, us.fechaActualiza" +
                            " from Usuario us where us.estado = 1 and  us.Id = " + idUsuario;

            using (SqlConnection sqlConnection = EstaticoSistema.devolverInstanciaSQLConnection())
            {
                using (SqlCommand cmd = new SqlCommand(sentencia, sqlConnection))
                {
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet dt = new DataSet();
                    da.Fill(dt);

                    if (dt != null && dt.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow itemtabla in dt.Tables[0].Rows)
                        {

                            usuarioRespuesta.Id = (int)itemtabla[0];
                            usuarioRespuesta.nombreUsuario = (string)itemtabla[1];
                            usuarioRespuesta.claveUsuario = (string)itemtabla[2];
                            try
                            {
                                usuarioRespuesta.imagenUsuario = EstaticoSistema.ByteArrayToImage((byte[])itemtabla[3]);
                            }
                            catch
                            {
                                usuarioRespuesta.imagenUsuario = null;
                            }
                            if (opcionDetalle == 1)
                            {
                                usuarioRespuesta.usuarioCrea = obtenerUsuarioById((int)itemtabla[4], 0);
                                usuarioRespuesta.usuarioActualiza = obtenerUsuarioById((int)itemtabla[5], 0);
                            }
                            else
                            {
                                usuarioRespuesta.usuarioCrea = null;
                                usuarioRespuesta.usuarioActualiza = null;
                            }
                            usuarioRespuesta.fechaCreacion = (DateTime)itemtabla[6];
                            usuarioRespuesta.fechaActualiza = (DateTime)itemtabla[7];
                            usuarioRespuesta.exito = true;
                        }
                    }
                    else {
                        usuarioRespuesta.respuesta = "Usuario no existe o está inactivo";
                    }
                }
            }

            return usuarioRespuesta;
        }

        public List<Usuario> obtenerUsuariosLista(int opc)
        {
            List<Usuario> usuariosRespuesta = new List<Usuario>();
            string sentencia = "select us.Id, us.nombreUsuario, us.claveUsuario, us.imagenUsuario," +
                            " us.usuarioCrea, us.usuarioActualiza, us.fechaCrea, us.fechaActualiza" +
                            " isnull(usc.nombreUsuario,'') as usuarioCreaNombre," +
                            " isnull(usc2.nombreUsuario,'') as usuarioCreaNombre2" +
                            " from Usuario us" +
                            " left join Usuario usc on usc.Id = us.UsuarioCrea" +
                            " left join Usuario usc2 on usc2.Id = us.UsuarioActualiza";

            if (opc <= 1)
            {
                sentencia += " where us.estado = " + opc.ToString();
            }

            using (SqlConnection sqlConnection = EstaticoSistema.devolverInstanciaSQLConnection())
            {
                using (SqlCommand cmd = new SqlCommand(sentencia, sqlConnection))
                {
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet dt = new DataSet();
                    da.Fill(dt);

                    if (dt != null && dt.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow itemtabla in dt.Tables[0].Rows)
                        {
                            Usuario usuario = new Usuario();
                            usuario.Id = (int)itemtabla[0];
                            usuario.nombreUsuario = (string)itemtabla[1];
                            usuario.claveUsuario = (string)itemtabla[2];
                            try
                            {
                                usuario.imagenUsuario = EstaticoSistema.ByteArrayToImage((byte[])itemtabla[3]);
                            }
                            catch
                            {
                                usuario.imagenUsuario = null;
                            }
                            usuario.usuarioCreaId = (int)itemtabla[4];
                            usuario.usuarioActualizaId = (int)itemtabla[5];
                            usuario.fechaCreacion = (DateTime)itemtabla[6];
                            usuario.fechaActualiza = (DateTime)itemtabla[7];
                            usuario.usuarioCreaDescripcion = (string)itemtabla[8];
                            usuario.usuarioActualizaDescripcion = (string)itemtabla[9];
                            usuario.exito = true;
                        }
                    }
                }
            }
            return usuariosRespuesta;
        }
    }
}
