﻿namespace LetacoSorteo
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.pnlPrincipal = new System.Windows.Forms.Panel();
            this.lblfechaHora = new System.Windows.Forms.Label();
            this.splitContenedorPrincipal = new System.Windows.Forms.SplitContainer();
            this.lblNombreUsuario = new System.Windows.Forms.Label();
            this.pbxImagenUsuario = new System.Windows.Forms.PictureBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.lblReporteVentas = new System.Windows.Forms.Label();
            this.pbxReporteVentas = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblConfiguracion = new System.Windows.Forms.Label();
            this.pbxConfiguracion = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblCrearSorteo = new System.Windows.Forms.Label();
            this.pbxCrearSorteo = new System.Windows.Forms.PictureBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.lblReportes = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblsorteo = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblusuarios = new System.Windows.Forms.Label();
            this.pbxUsuarios = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblProductos = new System.Windows.Forms.Label();
            this.pbxProductos = new System.Windows.Forms.PictureBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.lblHistorialSorteo = new System.Windows.Forms.Label();
            this.pbxHistorialSorteo = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lblHistorialParticipante = new System.Windows.Forms.Label();
            this.pbxHistorialParticipante = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblRealizarSorteo = new System.Windows.Forms.Label();
            this.pbxRealizarSorteo = new System.Windows.Forms.PictureBox();
            this.gbxOperaciones = new System.Windows.Forms.GroupBox();
            this.btnminimizar = new System.Windows.Forms.Button();
            this.btncerrar = new System.Windows.Forms.Button();
            this.pnlPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContenedorPrincipal)).BeginInit();
            this.splitContenedorPrincipal.Panel1.SuspendLayout();
            this.splitContenedorPrincipal.Panel2.SuspendLayout();
            this.splitContenedorPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenUsuario)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxReporteVentas)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxConfiguracion)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCrearSorteo)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxUsuarios)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxProductos)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHistorialSorteo)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHistorialParticipante)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxRealizarSorteo)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlPrincipal
            // 
            this.pnlPrincipal.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.pnlPrincipal.Controls.Add(this.lblfechaHora);
            this.pnlPrincipal.Controls.Add(this.splitContenedorPrincipal);
            this.pnlPrincipal.Controls.Add(this.btnminimizar);
            this.pnlPrincipal.Controls.Add(this.btncerrar);
            this.pnlPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPrincipal.Location = new System.Drawing.Point(0, 0);
            this.pnlPrincipal.Name = "pnlPrincipal";
            this.pnlPrincipal.Size = new System.Drawing.Size(927, 788);
            this.pnlPrincipal.TabIndex = 0;
            // 
            // lblfechaHora
            // 
            this.lblfechaHora.AutoSize = true;
            this.lblfechaHora.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.lblfechaHora.Location = new System.Drawing.Point(13, 13);
            this.lblfechaHora.Name = "lblfechaHora";
            this.lblfechaHora.Size = new System.Drawing.Size(89, 37);
            this.lblfechaHora.TabIndex = 8;
            this.lblfechaHora.Text = "label1";
            // 
            // splitContenedorPrincipal
            // 
            this.splitContenedorPrincipal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContenedorPrincipal.Location = new System.Drawing.Point(13, 56);
            this.splitContenedorPrincipal.Name = "splitContenedorPrincipal";
            // 
            // splitContenedorPrincipal.Panel1
            // 
            this.splitContenedorPrincipal.Panel1.Controls.Add(this.lblNombreUsuario);
            this.splitContenedorPrincipal.Panel1.Controls.Add(this.pbxImagenUsuario);
            this.splitContenedorPrincipal.Panel1.Controls.Add(this.panel10);
            this.splitContenedorPrincipal.Panel1.Controls.Add(this.panel1);
            this.splitContenedorPrincipal.Panel1.Controls.Add(this.panel5);
            this.splitContenedorPrincipal.Panel1.Controls.Add(this.panel9);
            this.splitContenedorPrincipal.Panel1.Controls.Add(this.panel6);
            this.splitContenedorPrincipal.Panel1.Controls.Add(this.panel2);
            this.splitContenedorPrincipal.Panel1.Controls.Add(this.panel3);
            this.splitContenedorPrincipal.Panel1.Controls.Add(this.panel8);
            this.splitContenedorPrincipal.Panel1.Controls.Add(this.panel7);
            this.splitContenedorPrincipal.Panel1.Controls.Add(this.panel4);
            // 
            // splitContenedorPrincipal.Panel2
            // 
            this.splitContenedorPrincipal.Panel2.Controls.Add(this.gbxOperaciones);
            this.splitContenedorPrincipal.Size = new System.Drawing.Size(900, 720);
            this.splitContenedorPrincipal.SplitterDistance = 270;
            this.splitContenedorPrincipal.TabIndex = 7;
            this.splitContenedorPrincipal.Text = "splitContainer1";
            // 
            // lblNombreUsuario
            // 
            this.lblNombreUsuario.AutoSize = true;
            this.lblNombreUsuario.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.lblNombreUsuario.Location = new System.Drawing.Point(53, 21);
            this.lblNombreUsuario.Name = "lblNombreUsuario";
            this.lblNombreUsuario.Size = new System.Drawing.Size(105, 15);
            this.lblNombreUsuario.TabIndex = 3;
            this.lblNombreUsuario.Text = "UsuarioLogueado";
            // 
            // pbxImagenUsuario
            // 
            this.pbxImagenUsuario.Image = ((System.Drawing.Image)(resources.GetObject("pbxImagenUsuario.Image")));
            this.pbxImagenUsuario.Location = new System.Drawing.Point(13, 13);
            this.pbxImagenUsuario.Name = "pbxImagenUsuario";
            this.pbxImagenUsuario.Size = new System.Drawing.Size(38, 33);
            this.pbxImagenUsuario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxImagenUsuario.TabIndex = 2;
            this.pbxImagenUsuario.TabStop = false;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.lblReporteVentas);
            this.panel10.Controls.Add(this.pbxReporteVentas);
            this.panel10.Location = new System.Drawing.Point(84, 603);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(275, 50);
            this.panel10.TabIndex = 1;
            // 
            // lblReporteVentas
            // 
            this.lblReporteVentas.AutoSize = true;
            this.lblReporteVentas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblReporteVentas.Font = new System.Drawing.Font("Segoe UI", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.lblReporteVentas.Location = new System.Drawing.Point(58, 11);
            this.lblReporteVentas.Name = "lblReporteVentas";
            this.lblReporteVentas.Size = new System.Drawing.Size(183, 28);
            this.lblReporteVentas.TabIndex = 8;
            this.lblReporteVentas.Tag = "frmReporteVentas";
            this.lblReporteVentas.Text = "Reporte de Ventas";
            // 
            // pbxReporteVentas
            // 
            this.pbxReporteVentas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxReporteVentas.Image = ((System.Drawing.Image)(resources.GetObject("pbxReporteVentas.Image")));
            this.pbxReporteVentas.Location = new System.Drawing.Point(7, 0);
            this.pbxReporteVentas.Name = "pbxReporteVentas";
            this.pbxReporteVentas.Size = new System.Drawing.Size(50, 50);
            this.pbxReporteVentas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxReporteVentas.TabIndex = 0;
            this.pbxReporteVentas.TabStop = false;
            this.pbxReporteVentas.Tag = "frmReporteVentas";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblConfiguracion);
            this.panel1.Controls.Add(this.pbxConfiguracion);
            this.panel1.Location = new System.Drawing.Point(13, 69);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(275, 60);
            this.panel1.TabIndex = 1;
            // 
            // lblConfiguracion
            // 
            this.lblConfiguracion.AutoSize = true;
            this.lblConfiguracion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblConfiguracion.Font = new System.Drawing.Font("Segoe UI", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.lblConfiguracion.Location = new System.Drawing.Point(71, 12);
            this.lblConfiguracion.Name = "lblConfiguracion";
            this.lblConfiguracion.Size = new System.Drawing.Size(197, 37);
            this.lblConfiguracion.TabIndex = 8;
            this.lblConfiguracion.Text = "Configuración";
            // 
            // pbxConfiguracion
            // 
            this.pbxConfiguracion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxConfiguracion.Image = ((System.Drawing.Image)(resources.GetObject("pbxConfiguracion.Image")));
            this.pbxConfiguracion.Location = new System.Drawing.Point(7, 0);
            this.pbxConfiguracion.Name = "pbxConfiguracion";
            this.pbxConfiguracion.Size = new System.Drawing.Size(60, 60);
            this.pbxConfiguracion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxConfiguracion.TabIndex = 0;
            this.pbxConfiguracion.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lblCrearSorteo);
            this.panel5.Controls.Add(this.pbxCrearSorteo);
            this.panel5.Location = new System.Drawing.Point(84, 314);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(275, 50);
            this.panel5.TabIndex = 1;
            // 
            // lblCrearSorteo
            // 
            this.lblCrearSorteo.AutoSize = true;
            this.lblCrearSorteo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblCrearSorteo.Font = new System.Drawing.Font("Segoe UI", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.lblCrearSorteo.Location = new System.Drawing.Point(58, 11);
            this.lblCrearSorteo.Name = "lblCrearSorteo";
            this.lblCrearSorteo.Size = new System.Drawing.Size(130, 28);
            this.lblCrearSorteo.TabIndex = 8;
            this.lblCrearSorteo.Tag = "frmCrearSorteo";
            this.lblCrearSorteo.Text = "Crear Sorteo";
            // 
            // pbxCrearSorteo
            // 
            this.pbxCrearSorteo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxCrearSorteo.Image = ((System.Drawing.Image)(resources.GetObject("pbxCrearSorteo.Image")));
            this.pbxCrearSorteo.Location = new System.Drawing.Point(7, 0);
            this.pbxCrearSorteo.Name = "pbxCrearSorteo";
            this.pbxCrearSorteo.Size = new System.Drawing.Size(50, 50);
            this.pbxCrearSorteo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxCrearSorteo.TabIndex = 0;
            this.pbxCrearSorteo.TabStop = false;
            this.pbxCrearSorteo.Tag = "frmCrearSorteo";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.lblReportes);
            this.panel9.Controls.Add(this.pictureBox4);
            this.panel9.Location = new System.Drawing.Point(13, 436);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(275, 60);
            this.panel9.TabIndex = 1;
            // 
            // lblReportes
            // 
            this.lblReportes.AutoSize = true;
            this.lblReportes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblReportes.Font = new System.Drawing.Font("Segoe UI", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.lblReportes.Location = new System.Drawing.Point(71, 12);
            this.lblReportes.Name = "lblReportes";
            this.lblReportes.Size = new System.Drawing.Size(127, 37);
            this.lblReportes.TabIndex = 8;
            this.lblReportes.Text = "Reportes";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(7, 0);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(60, 60);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.lblsorteo);
            this.panel6.Controls.Add(this.pictureBox3);
            this.panel6.Location = new System.Drawing.Point(13, 251);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(275, 60);
            this.panel6.TabIndex = 1;
            // 
            // lblsorteo
            // 
            this.lblsorteo.AutoSize = true;
            this.lblsorteo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblsorteo.Font = new System.Drawing.Font("Segoe UI", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.lblsorteo.Location = new System.Drawing.Point(71, 12);
            this.lblsorteo.Name = "lblsorteo";
            this.lblsorteo.Size = new System.Drawing.Size(99, 37);
            this.lblsorteo.TabIndex = 8;
            this.lblsorteo.Text = "Sorteo";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(7, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(60, 60);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblusuarios);
            this.panel2.Controls.Add(this.pbxUsuarios);
            this.panel2.Location = new System.Drawing.Point(84, 131);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(275, 50);
            this.panel2.TabIndex = 1;
            // 
            // lblusuarios
            // 
            this.lblusuarios.AutoSize = true;
            this.lblusuarios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblusuarios.Font = new System.Drawing.Font("Segoe UI", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.lblusuarios.Location = new System.Drawing.Point(58, 11);
            this.lblusuarios.Name = "lblusuarios";
            this.lblusuarios.Size = new System.Drawing.Size(94, 28);
            this.lblusuarios.TabIndex = 8;
            this.lblusuarios.Tag = "frmUsuarios";
            this.lblusuarios.Text = "Usuarios";
            // 
            // pbxUsuarios
            // 
            this.pbxUsuarios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxUsuarios.Image = ((System.Drawing.Image)(resources.GetObject("pbxUsuarios.Image")));
            this.pbxUsuarios.Location = new System.Drawing.Point(7, 0);
            this.pbxUsuarios.Name = "pbxUsuarios";
            this.pbxUsuarios.Size = new System.Drawing.Size(50, 50);
            this.pbxUsuarios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxUsuarios.TabIndex = 0;
            this.pbxUsuarios.TabStop = false;
            this.pbxUsuarios.Tag = "frmUsuarios";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblProductos);
            this.panel3.Controls.Add(this.pbxProductos);
            this.panel3.Location = new System.Drawing.Point(84, 182);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(275, 50);
            this.panel3.TabIndex = 1;
            // 
            // lblProductos
            // 
            this.lblProductos.AutoSize = true;
            this.lblProductos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblProductos.Font = new System.Drawing.Font("Segoe UI", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.lblProductos.Location = new System.Drawing.Point(58, 11);
            this.lblProductos.Name = "lblProductos";
            this.lblProductos.Size = new System.Drawing.Size(107, 28);
            this.lblProductos.TabIndex = 8;
            this.lblProductos.Tag = "frmProductos";
            this.lblProductos.Text = "Productos";
            // 
            // pbxProductos
            // 
            this.pbxProductos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxProductos.Image = ((System.Drawing.Image)(resources.GetObject("pbxProductos.Image")));
            this.pbxProductos.Location = new System.Drawing.Point(7, 0);
            this.pbxProductos.Name = "pbxProductos";
            this.pbxProductos.Size = new System.Drawing.Size(50, 50);
            this.pbxProductos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxProductos.TabIndex = 0;
            this.pbxProductos.TabStop = false;
            this.pbxProductos.Tag = "frmProductos";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.lblHistorialSorteo);
            this.panel8.Controls.Add(this.pbxHistorialSorteo);
            this.panel8.Location = new System.Drawing.Point(84, 500);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(275, 50);
            this.panel8.TabIndex = 1;
            // 
            // lblHistorialSorteo
            // 
            this.lblHistorialSorteo.AutoSize = true;
            this.lblHistorialSorteo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblHistorialSorteo.Font = new System.Drawing.Font("Segoe UI", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.lblHistorialSorteo.Location = new System.Drawing.Point(58, 11);
            this.lblHistorialSorteo.Name = "lblHistorialSorteo";
            this.lblHistorialSorteo.Size = new System.Drawing.Size(198, 28);
            this.lblHistorialSorteo.TabIndex = 8;
            this.lblHistorialSorteo.Tag = "frmHistorialSorteo";
            this.lblHistorialSorteo.Text = "Historial de Sorteos";
            // 
            // pbxHistorialSorteo
            // 
            this.pbxHistorialSorteo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxHistorialSorteo.Image = ((System.Drawing.Image)(resources.GetObject("pbxHistorialSorteo.Image")));
            this.pbxHistorialSorteo.Location = new System.Drawing.Point(7, 0);
            this.pbxHistorialSorteo.Name = "pbxHistorialSorteo";
            this.pbxHistorialSorteo.Size = new System.Drawing.Size(50, 50);
            this.pbxHistorialSorteo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxHistorialSorteo.TabIndex = 0;
            this.pbxHistorialSorteo.TabStop = false;
            this.pbxHistorialSorteo.Tag = "frmHistorialSorteo";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.lblHistorialParticipante);
            this.panel7.Controls.Add(this.pbxHistorialParticipante);
            this.panel7.Location = new System.Drawing.Point(84, 551);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(331, 50);
            this.panel7.TabIndex = 1;
            // 
            // lblHistorialParticipante
            // 
            this.lblHistorialParticipante.AutoSize = true;
            this.lblHistorialParticipante.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblHistorialParticipante.Font = new System.Drawing.Font("Segoe UI", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.lblHistorialParticipante.Location = new System.Drawing.Point(58, 11);
            this.lblHistorialParticipante.Name = "lblHistorialParticipante";
            this.lblHistorialParticipante.Size = new System.Drawing.Size(253, 28);
            this.lblHistorialParticipante.TabIndex = 8;
            this.lblHistorialParticipante.Tag = "frmHistorialParticipante";
            this.lblHistorialParticipante.Text = "Historial de Participantes";
            // 
            // pbxHistorialParticipante
            // 
            this.pbxHistorialParticipante.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxHistorialParticipante.Image = ((System.Drawing.Image)(resources.GetObject("pbxHistorialParticipante.Image")));
            this.pbxHistorialParticipante.Location = new System.Drawing.Point(7, 0);
            this.pbxHistorialParticipante.Name = "pbxHistorialParticipante";
            this.pbxHistorialParticipante.Size = new System.Drawing.Size(50, 50);
            this.pbxHistorialParticipante.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxHistorialParticipante.TabIndex = 0;
            this.pbxHistorialParticipante.TabStop = false;
            this.pbxHistorialParticipante.Tag = "frmHistorialParticipante";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lblRealizarSorteo);
            this.panel4.Controls.Add(this.pbxRealizarSorteo);
            this.panel4.Location = new System.Drawing.Point(84, 365);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(275, 50);
            this.panel4.TabIndex = 1;
            // 
            // lblRealizarSorteo
            // 
            this.lblRealizarSorteo.AutoSize = true;
            this.lblRealizarSorteo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRealizarSorteo.Font = new System.Drawing.Font("Segoe UI", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.lblRealizarSorteo.Location = new System.Drawing.Point(58, 11);
            this.lblRealizarSorteo.Name = "lblRealizarSorteo";
            this.lblRealizarSorteo.Size = new System.Drawing.Size(156, 28);
            this.lblRealizarSorteo.TabIndex = 8;
            this.lblRealizarSorteo.Tag = "frmRealizarSorteo";
            this.lblRealizarSorteo.Text = "Realizar Sorteo";
            // 
            // pbxRealizarSorteo
            // 
            this.pbxRealizarSorteo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxRealizarSorteo.Image = ((System.Drawing.Image)(resources.GetObject("pbxRealizarSorteo.Image")));
            this.pbxRealizarSorteo.Location = new System.Drawing.Point(7, 0);
            this.pbxRealizarSorteo.Name = "pbxRealizarSorteo";
            this.pbxRealizarSorteo.Size = new System.Drawing.Size(50, 50);
            this.pbxRealizarSorteo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxRealizarSorteo.TabIndex = 0;
            this.pbxRealizarSorteo.TabStop = false;
            this.pbxRealizarSorteo.Tag = "frmRealizarSorteo";
            // 
            // gbxOperaciones
            // 
            this.gbxOperaciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxOperaciones.Location = new System.Drawing.Point(0, 0);
            this.gbxOperaciones.Name = "gbxOperaciones";
            this.gbxOperaciones.Size = new System.Drawing.Size(626, 720);
            this.gbxOperaciones.TabIndex = 0;
            this.gbxOperaciones.TabStop = false;
            // 
            // btnminimizar
            // 
            this.btnminimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnminimizar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnminimizar.BackgroundImage")));
            this.btnminimizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnminimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnminimizar.FlatAppearance.BorderSize = 0;
            this.btnminimizar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnminimizar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnminimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnminimizar.Location = new System.Drawing.Point(822, 12);
            this.btnminimizar.Name = "btnminimizar";
            this.btnminimizar.Size = new System.Drawing.Size(41, 38);
            this.btnminimizar.TabIndex = 6;
            this.btnminimizar.UseVisualStyleBackColor = true;
            this.btnminimizar.Click += new System.EventHandler(this.btnminimizar_Click);
            // 
            // btncerrar
            // 
            this.btncerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btncerrar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btncerrar.BackgroundImage")));
            this.btncerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btncerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btncerrar.FlatAppearance.BorderSize = 0;
            this.btncerrar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btncerrar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btncerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncerrar.Location = new System.Drawing.Point(871, 12);
            this.btncerrar.Name = "btncerrar";
            this.btncerrar.Size = new System.Drawing.Size(41, 38);
            this.btncerrar.TabIndex = 5;
            this.btncerrar.UseVisualStyleBackColor = true;
            this.btncerrar.Click += new System.EventHandler(this.btnsalir_Click);
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(927, 788);
            this.Controls.Add(this.pnlPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Principal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Principal_Load);
            this.pnlPrincipal.ResumeLayout(false);
            this.pnlPrincipal.PerformLayout();
            this.splitContenedorPrincipal.Panel1.ResumeLayout(false);
            this.splitContenedorPrincipal.Panel1.PerformLayout();
            this.splitContenedorPrincipal.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContenedorPrincipal)).EndInit();
            this.splitContenedorPrincipal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenUsuario)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxReporteVentas)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxConfiguracion)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCrearSorteo)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxUsuarios)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxProductos)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHistorialSorteo)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHistorialParticipante)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxRealizarSorteo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlPrincipal;
        private System.Windows.Forms.Button btnminimizar;
        private System.Windows.Forms.Button btncerrar;
        private System.Windows.Forms.SplitContainer splitContenedorPrincipal;
        private System.Windows.Forms.GroupBox gbxOperaciones;
        private System.Windows.Forms.Label lblfechaHora;
        private System.Windows.Forms.Label lblConfiguracion;
        private System.Windows.Forms.PictureBox pbxConfiguracion;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblusuarios;
        private System.Windows.Forms.PictureBox pbxUsuarios;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblProductos;
        private System.Windows.Forms.PictureBox pbxProductos;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblsorteo;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblCrearSorteo;
        private System.Windows.Forms.PictureBox pbxCrearSorteo;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblRealizarSorteo;
        private System.Windows.Forms.PictureBox pbxRealizarSorteo;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label lblReportes;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label lblHistorialSorteo;
        private System.Windows.Forms.PictureBox pbxHistorialSorteo;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label lblHistorialParticipante;
        private System.Windows.Forms.PictureBox pbxHistorialParticipante;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label lblReporteVentas;
        private System.Windows.Forms.PictureBox pbxReporteVentas;
        private System.Windows.Forms.PictureBox pbxImagenUsuario;
        private System.Windows.Forms.Label lblNombreUsuario;
    }
}