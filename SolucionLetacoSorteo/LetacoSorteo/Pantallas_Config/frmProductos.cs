﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LetacoSorteo
{
    public partial class frmProductos : Form
    {
        public frmProductos()
        {
            InitializeComponent();
        }

        private void btnExaminar_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "*.jpg|*.jpg";
            openFileDialog.Title = "Seleccione Imagen para Usuario " + txtNombreProducto.Text;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                pbxImagenProducto.Image = Image.FromFile(openFileDialog.FileName);
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            pbxImagenProducto.Image = LetacoSorteo.Properties.Resources.SinImagen;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            EstaticoSistema.formularioscargados.Remove(this.Name);
            this.Close();
        }
    }
}
