﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LetacoSorteo
{
    public partial class frmUsuarios : Form
    {
        public frmUsuarios()
        {
            InitializeComponent();
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            EstaticoSistema.formularioscargados.Remove(this.Name);
            this.Close();
        }

        private void pbxverclave_MouseMove(object sender, MouseEventArgs e)
        {
            txtClave.UseSystemPasswordChar = false;
        }

        private void pbxverclave_MouseLeave(object sender, EventArgs e)
        {
            txtClave.UseSystemPasswordChar = true;
        }

        private void btnExaminar_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "*.jpg|*.jpg";
            openFileDialog.Title = "Seleccione Imagen para Usuario " + txtUsuario.Text;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                pbxImagenUsuario.Image = Image.FromFile(openFileDialog.FileName);
            }
        }

        private void btnLimpiarImagen_Click(object sender, EventArgs e)
        {
            pbxImagenUsuario.Image = LetacoSorteo.Properties.Resources.SinImagen;
        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            txtUsuario.Focus();
        }
    }
}
