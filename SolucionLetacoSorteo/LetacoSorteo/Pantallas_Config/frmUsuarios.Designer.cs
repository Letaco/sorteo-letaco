﻿namespace LetacoSorteo
{
    partial class frmUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUsuarios));
            this.btnCerrar = new System.Windows.Forms.Button();
            this.gbxListaUsuarios = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtFiltro = new System.Windows.Forms.TextBox();
            this.dgvUsuarios = new System.Windows.Forms.DataGridView();
            this.nombreUsuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaCreacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estadoDescrip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbxCrearEditar = new System.Windows.Forms.GroupBox();
            this.btnLimpiarImagen = new System.Windows.Forms.Button();
            this.btnCrearEditar = new System.Windows.Forms.Button();
            this.btnExaminar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pbxImagenUsuario = new System.Windows.Forms.PictureBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtClave = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pbxVerClave = new System.Windows.Forms.PictureBox();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.gbxPermisos = new System.Windows.Forms.GroupBox();
            this.cbxTipoPermiso = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvPermisoUsuario = new System.Windows.Forms.DataGridView();
            this.usuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.permiso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoPermiso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnEliminarPermisoUsuario = new System.Windows.Forms.Button();
            this.btnAgregarPermisoUsuario = new System.Windows.Forms.Button();
            this.cbxPermisos = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbxUsuarios = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.gbxEliminarUsuario = new System.Windows.Forms.GroupBox();
            this.cbxUsuarioEliminar = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.gbxListaUsuarios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).BeginInit();
            this.gbxCrearEditar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxVerClave)).BeginInit();
            this.gbxPermisos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPermisoUsuario)).BeginInit();
            this.gbxEliminarUsuario.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCerrar
            // 
            this.btnCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCerrar.BackgroundImage")));
            this.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCerrar.FlatAppearance.BorderSize = 0;
            this.btnCerrar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnCerrar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrar.Location = new System.Drawing.Point(871, 9);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(41, 38);
            this.btnCerrar.TabIndex = 5;
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnsalir_Click);
            // 
            // gbxListaUsuarios
            // 
            this.gbxListaUsuarios.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxListaUsuarios.Controls.Add(this.label4);
            this.gbxListaUsuarios.Controls.Add(this.txtFiltro);
            this.gbxListaUsuarios.Controls.Add(this.dgvUsuarios);
            this.gbxListaUsuarios.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.gbxListaUsuarios.Location = new System.Drawing.Point(526, 265);
            this.gbxListaUsuarios.Name = "gbxListaUsuarios";
            this.gbxListaUsuarios.Size = new System.Drawing.Size(381, 280);
            this.gbxListaUsuarios.TabIndex = 7;
            this.gbxListaUsuarios.TabStop = false;
            this.gbxListaUsuarios.Text = "Listado de Usuarios";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(19, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 19);
            this.label4.TabIndex = 0;
            this.label4.Text = "Filtro";
            // 
            // txtFiltro
            // 
            this.txtFiltro.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFiltro.Font = new System.Drawing.Font("Segoe UI", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.txtFiltro.Location = new System.Drawing.Point(68, 28);
            this.txtFiltro.Name = "txtFiltro";
            this.txtFiltro.Size = new System.Drawing.Size(290, 25);
            this.txtFiltro.TabIndex = 2;
            // 
            // dgvUsuarios
            // 
            this.dgvUsuarios.AllowUserToAddRows = false;
            this.dgvUsuarios.AllowUserToDeleteRows = false;
            this.dgvUsuarios.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvUsuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUsuarios.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nombreUsuario,
            this.fechaCreacion,
            this.estadoDescrip});
            this.dgvUsuarios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgvUsuarios.Location = new System.Drawing.Point(6, 66);
            this.dgvUsuarios.Name = "dgvUsuarios";
            this.dgvUsuarios.ReadOnly = true;
            this.dgvUsuarios.RowHeadersVisible = false;
            this.dgvUsuarios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUsuarios.ShowEditingIcon = false;
            this.dgvUsuarios.Size = new System.Drawing.Size(369, 208);
            this.dgvUsuarios.TabIndex = 0;
            this.dgvUsuarios.Text = "dataGridView1";
            // 
            // nombreUsuario
            // 
            this.nombreUsuario.DataPropertyName = "nombreUsuario";
            this.nombreUsuario.HeaderText = "Usuario";
            this.nombreUsuario.Name = "nombreUsuario";
            this.nombreUsuario.ReadOnly = true;
            this.nombreUsuario.Width = 150;
            // 
            // fechaCreacion
            // 
            this.fechaCreacion.DataPropertyName = "fechaCreacion";
            this.fechaCreacion.HeaderText = "Creación";
            this.fechaCreacion.Name = "fechaCreacion";
            this.fechaCreacion.ReadOnly = true;
            // 
            // estadoDescrip
            // 
            this.estadoDescrip.DataPropertyName = "estadoDescrip";
            this.estadoDescrip.HeaderText = "Estado";
            this.estadoDescrip.Name = "estadoDescrip";
            this.estadoDescrip.ReadOnly = true;
            // 
            // gbxCrearEditar
            // 
            this.gbxCrearEditar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxCrearEditar.Controls.Add(this.btnLimpiarImagen);
            this.gbxCrearEditar.Controls.Add(this.btnCrearEditar);
            this.gbxCrearEditar.Controls.Add(this.btnExaminar);
            this.gbxCrearEditar.Controls.Add(this.label2);
            this.gbxCrearEditar.Controls.Add(this.pbxImagenUsuario);
            this.gbxCrearEditar.Controls.Add(this.txtUsuario);
            this.gbxCrearEditar.Controls.Add(this.txtClave);
            this.gbxCrearEditar.Controls.Add(this.label1);
            this.gbxCrearEditar.Controls.Add(this.pbxVerClave);
            this.gbxCrearEditar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.gbxCrearEditar.Location = new System.Drawing.Point(12, 9);
            this.gbxCrearEditar.Name = "gbxCrearEditar";
            this.gbxCrearEditar.Size = new System.Drawing.Size(502, 241);
            this.gbxCrearEditar.TabIndex = 8;
            this.gbxCrearEditar.TabStop = false;
            this.gbxCrearEditar.Text = "Agregar/Editar Usuario";
            // 
            // btnLimpiarImagen
            // 
            this.btnLimpiarImagen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLimpiarImagen.BackColor = System.Drawing.Color.Transparent;
            this.btnLimpiarImagen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLimpiarImagen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLimpiarImagen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpiarImagen.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnLimpiarImagen.Location = new System.Drawing.Point(367, 205);
            this.btnLimpiarImagen.Name = "btnLimpiarImagen";
            this.btnLimpiarImagen.Size = new System.Drawing.Size(120, 27);
            this.btnLimpiarImagen.TabIndex = 5;
            this.btnLimpiarImagen.Text = "Limpiar Imagen";
            this.btnLimpiarImagen.UseVisualStyleBackColor = false;
            this.btnLimpiarImagen.Click += new System.EventHandler(this.btnLimpiarImagen_Click);
            // 
            // btnCrearEditar
            // 
            this.btnCrearEditar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCrearEditar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCrearEditar.FlatAppearance.BorderSize = 0;
            this.btnCrearEditar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnCrearEditar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnCrearEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCrearEditar.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.btnCrearEditar.ForeColor = System.Drawing.Color.Black;
            this.btnCrearEditar.Location = new System.Drawing.Point(20, 157);
            this.btnCrearEditar.Name = "btnCrearEditar";
            this.btnCrearEditar.Size = new System.Drawing.Size(134, 40);
            this.btnCrearEditar.TabIndex = 4;
            this.btnCrearEditar.Text = "Crear Usuario";
            this.btnCrearEditar.UseVisualStyleBackColor = true;
            // 
            // btnExaminar
            // 
            this.btnExaminar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExaminar.BackColor = System.Drawing.Color.Transparent;
            this.btnExaminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExaminar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExaminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExaminar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnExaminar.Location = new System.Drawing.Point(367, 170);
            this.btnExaminar.Name = "btnExaminar";
            this.btnExaminar.Size = new System.Drawing.Size(120, 27);
            this.btnExaminar.TabIndex = 5;
            this.btnExaminar.Text = "Examinar ...";
            this.btnExaminar.UseVisualStyleBackColor = false;
            this.btnExaminar.Click += new System.EventHandler(this.btnExaminar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(16, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Usuario";
            // 
            // pbxImagenUsuario
            // 
            this.pbxImagenUsuario.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbxImagenUsuario.Cursor = System.Windows.Forms.Cursors.No;
            this.pbxImagenUsuario.Image = ((System.Drawing.Image)(resources.GetObject("pbxImagenUsuario.Image")));
            this.pbxImagenUsuario.Location = new System.Drawing.Point(367, 36);
            this.pbxImagenUsuario.Name = "pbxImagenUsuario";
            this.pbxImagenUsuario.Size = new System.Drawing.Size(120, 128);
            this.pbxImagenUsuario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImagenUsuario.TabIndex = 1;
            this.pbxImagenUsuario.TabStop = false;
            // 
            // txtUsuario
            // 
            this.txtUsuario.Font = new System.Drawing.Font("Segoe UI", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.txtUsuario.Location = new System.Drawing.Point(81, 45);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(274, 25);
            this.txtUsuario.TabIndex = 2;
            // 
            // txtClave
            // 
            this.txtClave.Font = new System.Drawing.Font("Segoe UI", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.txtClave.Location = new System.Drawing.Point(81, 97);
            this.txtClave.Name = "txtClave";
            this.txtClave.Size = new System.Drawing.Size(226, 25);
            this.txtClave.TabIndex = 3;
            this.txtClave.UseSystemPasswordChar = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(16, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Clave";
            // 
            // pbxVerClave
            // 
            this.pbxVerClave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxVerClave.Image = ((System.Drawing.Image)(resources.GetObject("pbxVerClave.Image")));
            this.pbxVerClave.Location = new System.Drawing.Point(313, 97);
            this.pbxVerClave.Name = "pbxVerClave";
            this.pbxVerClave.Size = new System.Drawing.Size(42, 25);
            this.pbxVerClave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxVerClave.TabIndex = 7;
            this.pbxVerClave.TabStop = false;
            this.pbxVerClave.MouseLeave += new System.EventHandler(this.pbxverclave_MouseLeave);
            this.pbxVerClave.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbxverclave_MouseMove);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEliminar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminar.FlatAppearance.BorderSize = 0;
            this.btnEliminar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnEliminar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.btnEliminar.ForeColor = System.Drawing.Color.Black;
            this.btnEliminar.Location = new System.Drawing.Point(119, 87);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(143, 40);
            this.btnEliminar.TabIndex = 4;
            this.btnEliminar.Text = "Eliminar Usuario";
            this.btnEliminar.UseVisualStyleBackColor = true;
            // 
            // gbxPermisos
            // 
            this.gbxPermisos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxPermisos.Controls.Add(this.cbxTipoPermiso);
            this.gbxPermisos.Controls.Add(this.label6);
            this.gbxPermisos.Controls.Add(this.dgvPermisoUsuario);
            this.gbxPermisos.Controls.Add(this.btnEliminarPermisoUsuario);
            this.gbxPermisos.Controls.Add(this.btnAgregarPermisoUsuario);
            this.gbxPermisos.Controls.Add(this.cbxPermisos);
            this.gbxPermisos.Controls.Add(this.label5);
            this.gbxPermisos.Controls.Add(this.cbxUsuarios);
            this.gbxPermisos.Controls.Add(this.label3);
            this.gbxPermisos.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.gbxPermisos.Location = new System.Drawing.Point(12, 265);
            this.gbxPermisos.Name = "gbxPermisos";
            this.gbxPermisos.Size = new System.Drawing.Size(502, 280);
            this.gbxPermisos.TabIndex = 7;
            this.gbxPermisos.TabStop = false;
            this.gbxPermisos.Text = "Permisos Usuario";
            // 
            // cbxTipoPermiso
            // 
            this.cbxTipoPermiso.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxTipoPermiso.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxTipoPermiso.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbxTipoPermiso.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.cbxTipoPermiso.FormattingEnabled = true;
            this.cbxTipoPermiso.Items.AddRange(new object[] {
            "Sólo Lectura",
            "Escritura"});
            this.cbxTipoPermiso.Location = new System.Drawing.Point(101, 106);
            this.cbxTipoPermiso.Name = "cbxTipoPermiso";
            this.cbxTipoPermiso.Size = new System.Drawing.Size(233, 25);
            this.cbxTipoPermiso.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(20, 107);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 19);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tipo";
            // 
            // dgvPermisoUsuario
            // 
            this.dgvPermisoUsuario.AllowUserToAddRows = false;
            this.dgvPermisoUsuario.AllowUserToDeleteRows = false;
            this.dgvPermisoUsuario.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPermisoUsuario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPermisoUsuario.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.usuario,
            this.permiso,
            this.tipoPermiso});
            this.dgvPermisoUsuario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgvPermisoUsuario.Location = new System.Drawing.Point(16, 149);
            this.dgvPermisoUsuario.Name = "dgvPermisoUsuario";
            this.dgvPermisoUsuario.ReadOnly = true;
            this.dgvPermisoUsuario.RowHeadersVisible = false;
            this.dgvPermisoUsuario.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPermisoUsuario.Size = new System.Drawing.Size(471, 122);
            this.dgvPermisoUsuario.TabIndex = 3;
            this.dgvPermisoUsuario.Text = "dataGridView1";
            // 
            // usuario
            // 
            this.usuario.DataPropertyName = "usuario";
            this.usuario.HeaderText = "Usuario";
            this.usuario.Name = "usuario";
            this.usuario.ReadOnly = true;
            this.usuario.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.usuario.Width = 150;
            // 
            // permiso
            // 
            this.permiso.DataPropertyName = "permiso";
            this.permiso.HeaderText = "Permiso";
            this.permiso.Name = "permiso";
            this.permiso.ReadOnly = true;
            this.permiso.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.permiso.Width = 150;
            // 
            // tipoPermiso
            // 
            this.tipoPermiso.DataPropertyName = "tipoPermiso";
            this.tipoPermiso.HeaderText = "Tipo de Permiso";
            this.tipoPermiso.Name = "tipoPermiso";
            this.tipoPermiso.ReadOnly = true;
            this.tipoPermiso.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tipoPermiso.Width = 150;
            // 
            // btnEliminarPermisoUsuario
            // 
            this.btnEliminarPermisoUsuario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminarPermisoUsuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarPermisoUsuario.Location = new System.Drawing.Point(367, 66);
            this.btnEliminarPermisoUsuario.Name = "btnEliminarPermisoUsuario";
            this.btnEliminarPermisoUsuario.Size = new System.Drawing.Size(120, 28);
            this.btnEliminarPermisoUsuario.TabIndex = 2;
            this.btnEliminarPermisoUsuario.Text = "Eliminar Permiso";
            this.btnEliminarPermisoUsuario.UseVisualStyleBackColor = true;
            // 
            // btnAgregarPermisoUsuario
            // 
            this.btnAgregarPermisoUsuario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAgregarPermisoUsuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarPermisoUsuario.Location = new System.Drawing.Point(367, 26);
            this.btnAgregarPermisoUsuario.Name = "btnAgregarPermisoUsuario";
            this.btnAgregarPermisoUsuario.Size = new System.Drawing.Size(120, 28);
            this.btnAgregarPermisoUsuario.TabIndex = 2;
            this.btnAgregarPermisoUsuario.Text = "Agregar Permiso";
            this.btnAgregarPermisoUsuario.UseVisualStyleBackColor = true;
            // 
            // cbxPermisos
            // 
            this.cbxPermisos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxPermisos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxPermisos.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbxPermisos.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.cbxPermisos.FormattingEnabled = true;
            this.cbxPermisos.Items.AddRange(new object[] {
            "Pantalla Usuarios",
            "Pantalla Productos",
            "Pantalla Crear Sorteo",
            "Pantalla Realizar Sorteo",
            "Pantalla Historial de Sorteos",
            "Pantalla Historial de Participantes",
            "Pantalla Reporte de Ventas"});
            this.cbxPermisos.Location = new System.Drawing.Point(101, 68);
            this.cbxPermisos.Name = "cbxPermisos";
            this.cbxPermisos.Size = new System.Drawing.Size(233, 25);
            this.cbxPermisos.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(20, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 19);
            this.label5.TabIndex = 0;
            this.label5.Text = "Permiso";
            // 
            // cbxUsuarios
            // 
            this.cbxUsuarios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxUsuarios.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxUsuarios.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbxUsuarios.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.cbxUsuarios.FormattingEnabled = true;
            this.cbxUsuarios.Location = new System.Drawing.Point(101, 30);
            this.cbxUsuarios.Name = "cbxUsuarios";
            this.cbxUsuarios.Size = new System.Drawing.Size(233, 25);
            this.cbxUsuarios.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(20, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 19);
            this.label3.TabIndex = 0;
            this.label3.Text = "Usuario";
            // 
            // gbxEliminarUsuario
            // 
            this.gbxEliminarUsuario.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxEliminarUsuario.Controls.Add(this.btnEliminar);
            this.gbxEliminarUsuario.Controls.Add(this.cbxUsuarioEliminar);
            this.gbxEliminarUsuario.Controls.Add(this.label7);
            this.gbxEliminarUsuario.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.gbxEliminarUsuario.Location = new System.Drawing.Point(526, 106);
            this.gbxEliminarUsuario.Name = "gbxEliminarUsuario";
            this.gbxEliminarUsuario.Size = new System.Drawing.Size(381, 144);
            this.gbxEliminarUsuario.TabIndex = 9;
            this.gbxEliminarUsuario.TabStop = false;
            this.gbxEliminarUsuario.Text = "Eliminar Usuario";
            // 
            // cbxUsuarioEliminar
            // 
            this.cbxUsuarioEliminar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxUsuarioEliminar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxUsuarioEliminar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbxUsuarioEliminar.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.cbxUsuarioEliminar.FormattingEnabled = true;
            this.cbxUsuarioEliminar.Location = new System.Drawing.Point(128, 38);
            this.cbxUsuarioEliminar.Name = "cbxUsuarioEliminar";
            this.cbxUsuarioEliminar.Size = new System.Drawing.Size(206, 25);
            this.cbxUsuarioEliminar.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(47, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 19);
            this.label7.TabIndex = 0;
            this.label7.Text = "Usuario";
            // 
            // frmUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientSize = new System.Drawing.Size(919, 557);
            this.Controls.Add(this.gbxEliminarUsuario);
            this.Controls.Add(this.gbxPermisos);
            this.Controls.Add(this.gbxCrearEditar);
            this.Controls.Add(this.gbxListaUsuarios);
            this.Controls.Add(this.btnCerrar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmUsuarios";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "frmUsuario";
            this.Load += new System.EventHandler(this.frmUsuarios_Load);
            this.gbxListaUsuarios.ResumeLayout(false);
            this.gbxListaUsuarios.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).EndInit();
            this.gbxCrearEditar.ResumeLayout(false);
            this.gbxCrearEditar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxVerClave)).EndInit();
            this.gbxPermisos.ResumeLayout(false);
            this.gbxPermisos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPermisoUsuario)).EndInit();
            this.gbxEliminarUsuario.ResumeLayout(false);
            this.gbxEliminarUsuario.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.GroupBox gbxListaUsuarios;
        private System.Windows.Forms.GroupBox gbxCrearEditar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtClave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pbxVerClave;
        private System.Windows.Forms.Button btnCrearEditar;
        private System.Windows.Forms.GroupBox gbxPermisos;
        private System.Windows.Forms.ComboBox cbxUsuarios;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtFiltro;
        private System.Windows.Forms.DataGridView dgvUsuarios;
        private System.Windows.Forms.ComboBox cbxPermisos;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnAgregarPermisoUsuario;
        private System.Windows.Forms.DataGridView dgvPermisoUsuario;
        private System.Windows.Forms.Button btnEliminarPermisoUsuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreUsuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaCreacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn estadoDescrip;
        private System.Windows.Forms.DataGridViewTextBoxColumn usuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn permiso;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoPermiso;
        private System.Windows.Forms.ComboBox cbxTipoPermiso;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox gbxEliminarUsuario;
        private System.Windows.Forms.ComboBox cbxUsuarioEliminar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnLimpiarImagen;
        private System.Windows.Forms.Button btnExaminar;
        private System.Windows.Forms.PictureBox pbxImagenUsuario;
    }
}